import java.util.ArrayList;
import java.util.Collections;

public class Series
{
    public void indicar()
    {
        ArrayList<String> lista = new ArrayList<>();
        lista.add("Velozes e Furiosos");
        lista.add("Vingadores");
        lista.add("Interestelar");
        lista.add("Creed");
        lista.add("Madrugada dos Mortos");
        lista.add("12 Anos de Escravidão");
        lista.add("Jogo da Imitação");
        lista.add("Scott Pilgrim");
        lista.add("Matrix");
        lista.add("Her");
        lista.add("V de Vingança");
        lista.add("Ted");
        lista.add("Forrest Gump");
        lista.add("O Contador");
        lista.add("Baby Driver");
        lista.add("Projeto X");
        lista.add("O Lobo de Wall Street");
        lista.add("Golpe Duplo");
        
        Collections.sort(lista, String::compareToIgnoreCase);
        for(String str : lista){
            System.out.println(str);
        }
        
        /*for(int i=0; i < filmesFavoritos.length; i++){
            System.out.println(filmesFavoritos[i]);
        }*/
    }
}
