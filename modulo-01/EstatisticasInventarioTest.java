import org.junit.Test;
import static org.junit.Assert.*;

public class EstatisticasInventarioTest {
    @Test
    public void calcularMediaDasQuantidadesDosItensComNumeroImpar()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));

        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        assertEquals(3.666666666666667, estatisticas.calcularMedia(), 1e-9);
    }

    @Test
    public void medianaDosItensComNumeroImpar()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));

        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        assertEquals(4, estatisticas.calcularMediana(), 1e-9);
    }

    @Test
    public void medianaDosItensComNumeroPar()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.adicionar(new Item(2, "Escudo"));

        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        assertEquals(3, estatisticas.calcularMediana(), 1e-9);
    }

    @Test
    public void qtdItensAcimaDaMedia()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.adicionar(new Item(3, "Escudo"));

        EstatisticasInventario estatisticas = new EstatisticasInventario(mochila);
        assertEquals(2, estatisticas.qtdItensAcimaDaMedia());
    }
}
