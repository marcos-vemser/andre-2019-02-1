public class Dwarf
{
    private String nome;
    private double vida;
    private Status status;
    private final int dano = 10;
    
    public Dwarf(String nome)
    {
        this.status = Status.RECEM_CRIADO;
        this.nome = nome;public class Dwarf
{
    private String nome;
    private double vida;
    private Status status;
    private final int dano = 10;

    public Dwarf(String nome)
    {
        this.nome = nome;
        this.vida = 110.0;
        this.status = Status.RECEM_CRIADO;
    }

    public String getNome()
    {
        return this.nome;
    }

    public double getVida()
    {
        return this.vida;
    }

    public Status getStatus()
    {
        return this.status;
    }

    public boolean podeSofrerDano()
    {
        return this.vida > 0;
    }

    public int matarDwarf()
    {
        this.status = Status.MORTO;
        return 0;
    }

    public void sofrerDano()
    {
        if(podeSofrerDano()){
            this.vida = (this.vida > dano) ? this.vida - dano : matarDwarf();
        }
    }
}

        vida = 110.0;
    }
    
    public String getNome()
    {
        return this.nome;
    }
    
    public double getVida()
    {
        return this.vida;
    }
    
    public boolean podeSofrerDano()
    {
        return this.vida > 0;
    }
    
    public int matarDwarf()
    {
        this.status = Status.MORTO;
        return 0;
    }
    
    public Status getStatus()
    {
        return this.status;
    }
    
     public void sofrerDano()
    {
        if(podeSofrerDano()){
            this.vida = (this.vida>dano) ? this.vida-dano : matarDwarf();
        }
    }  
    
}
