import static org.junit.Assert.*;
import org.junit.Test;

import java.util.ArrayList;

public class InventarioTest
{
    @Test
    public void criarInventario()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        assertEquals(4, mochila.obter(0).getQuantidade());
    }

    @Test
    public void getDescricaoDeTodosOsItens()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        assertEquals("Flecha,Arco,Faca", mochila.getDescricoesItens());
    }

    @Test
    public void getDescricaoNenhumItem()
    {
        Inventario mochila = new Inventario();
        assertEquals("", mochila.getDescricoesItens());
    }

    @Test
    public void obterObtemOItemDeUmaPosicao()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        assertEquals("Arco", mochila.obter(1).getDescricao());
    }

    @Test
    public void getItemComMaiorQuantidadeComVarios()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        assertEquals(6, mochila.getItemComMaiorQuantidade().getQuantidade());
    }

    @Test
    public void getItemComMaiorQuantidadeVazio()
    {
        Inventario mochila = new Inventario();
        assertNull(mochila.getItemComMaiorQuantidade());
    }

    @Test
    public void getItemComMaiorQuantidadeComItensComMesmaQuantidade()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(4, "Arco"));
        mochila.adicionar(new Item(4, "Faca"));
        assertEquals(4, mochila.getItemComMaiorQuantidade().getQuantidade());
        assertEquals("Flecha", mochila.getItemComMaiorQuantidade().getDescricao());
    }

    @Test
    public void getItemComMaiorQuantidadePrimeiroItemSendoNull()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.remover(0);
        assertEquals(6, mochila.getItemComMaiorQuantidade().getQuantidade());
        assertEquals("Faca", mochila.getItemComMaiorQuantidade().getDescricao());
    }

    @Test
    public void removerDefineComoNullOValorDoItemDeUmaPosicao()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.remover(0);
        assertNull(mochila.buscar("Flecha"));
    }

    @Test
    public void inverterItensDoArray()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        assertEquals("Faca", mochila.inverter().get(0).getDescricao());
        assertEquals("Arco", mochila.inverter().get(1).getDescricao());
        assertEquals("Flecha", mochila.inverter().get(2).getDescricao());
    }

    @Test
    public void buscarRetornaOPrimeiroItemDaListaPorADescricao()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        assertEquals("Arco", mochila.buscar("Arco").getDescricao());
    }

    @Test
    public void ordenaItens()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.ordenarItens();
        assertEquals("Arco,Flecha,Faca", mochila.getDescricoesItens());
    }

    @Test
    public void ordenaItensAscOrdenaItensOrdemCrescente()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.ordenarItens(TipoOrdenacao.ASC);
        assertEquals("Arco,Flecha,Faca", mochila.getDescricoesItens());
    }

    @Test
    public void ordenaItensDescOrdenaItensOrdemDecrescente()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(4, "Flecha"));
        mochila.adicionar(new Item(1, "Arco"));
        mochila.adicionar(new Item(6, "Faca"));
        mochila.ordenarItens(TipoOrdenacao.DESC);
        assertEquals("Faca,Flecha,Arco", mochila.getDescricoesItens());
    }

}
