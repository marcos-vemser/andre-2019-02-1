import org.junit.Test;
import static org.junit.Assert.assertEquals;

public class ElfoTest {

    @Test
    public void atirarFlechaDiminuirFlechaAumentarXp()
    {
        Elfo elfo1 = new Elfo("Legolas");
        Dwarf dwarf1 = new Dwarf("Gimli");
        elfo1.atirarFlecha(dwarf1);
        assertEquals(1, elfo1.getExperiencia());
        assertEquals(3, elfo1.getQtdFlechas());
    }

    @Test
    public void atirarFlechaLimiteInferiorQuantidadeFlechas()
    {
        Elfo elfo1 = new Elfo("Legolas");
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<6; i++)
            elfo1.atirarFlecha(dwarf1);
        //assertEquals(4, elfo1.getExperiencia());
        assertEquals(0, elfo1.getQtdFlechas());
    }

    @Test
    public void elfosNascemCom4Flechas()
    {
        Elfo elfo1 = new Elfo("Legolas");
        assertEquals(4, elfo1.getQtdFlechas());
    }

    @Test
    public void elfoPodeAtirarFlecha()
    {
        Elfo elfo1 = new Elfo("Legolas");
        elfo1.podeAtirarFlecha();
        assertEquals(true, elfo1.podeAtirarFlecha());
    }

    @Test
    public void getFlechaRetornaPrimeiroItemComDescricaoFlecha()
    {
        Elfo elfo1 = new Elfo("Legolas");
        assertEquals(4, elfo1.getQtdFlechas());
        assertEquals("Flecha", elfo1.getFlecha().getDescricao());
    }

    @Test
    public void elfoNaoPodeAtirarFlecha()
    {
        Elfo elfo1 = new Elfo("Legolas");
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<6; i++)
            elfo1.atirarFlecha(dwarf1);
        assertEquals(false, elfo1.podeAtirarFlecha());
    }

    @Test
    public void elfosNascemComStatusRecemCriado()
    {
        Elfo elfo1 = new Elfo("Legolas");
        assertEquals(Status.RECEM_CRIADO, elfo1.getStatus());
    }
}
