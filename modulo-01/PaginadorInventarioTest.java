import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class PaginadorInventarioTest
{
    @Test
    public void limitarPular0Limite2DeveExibirOsItensDasPosicoesEntre0E2()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(1, "Espada"));
        mochila.adicionar(new Item(2, "Escudo de Metal"));
        mochila.adicionar(new Item(3, "Poção de HP"));
        mochila.adicionar(new Item(4, "Bracelete"));

        PaginadorInventario paginador = new PaginadorInventario(mochila);
        paginador.pular(0);
        assertEquals("Espada", paginador.limitar(2).get(0).getDescricao());
        assertEquals("Escudo de Metal", paginador.limitar(2).get(1).getDescricao());
    }

    @Test
    public void limitarPular2Limite2DeveExibirOsItensDasPosicoesEntre2E4()
    {
        Inventario mochila = new Inventario();
        mochila.adicionar(new Item(1, "Espada"));
        mochila.adicionar(new Item(2, "Escudo"));
        mochila.adicionar(new Item(3, "Poção de HP"));
        mochila.adicionar(new Item(4, "Bracelete"));

        PaginadorInventario paginador = new PaginadorInventario(mochila);
        paginador.pular(2);
        assertEquals("Poção de HP", paginador.limitar(2).get(0).getDescricao());
        assertEquals("Bracelete", paginador.limitar(2).get(1).getDescricao());
    }
}
