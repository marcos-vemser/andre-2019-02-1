import java.util.ArrayList;

public class EstatisticasInventario {
    private Inventario mochila;
    public EstatisticasInventario(Inventario mochila)
    {
            this.mochila= mochila;
    }

    public double calcularMedia()
    {
        double soma = 0.0;
        ArrayList<Item> itens = mochila.getItens();
        for(int i=0; i<this.mochila.size(); i++) {
            soma += itens.get(i).getQuantidade();
        }
        return soma / mochila.size();
     }

     public double calcularMediana()
     {
         mochila.ordenarItens();
         ArrayList<Item> itens = mochila.getItens();
         if(mochila.size() % 2 == 0){
             return ( itens.get(mochila.size() / 2 -1).getQuantidade() + itens.get(mochila.size() / 2).getQuantidade() ) / 2;
         }
         else {
             return itens.get(mochila.size() / 2).getQuantidade();
         }
     }

    public int qtdItensAcimaDaMedia()
    {
        int qtdItens = 0;
        double mediaDosItens = calcularMedia();
        ArrayList<Item> itens = mochila.getItens();
        for(int i=0; i<this.mochila.size(); i++) {
            if(itens.get(i).getQuantidade() > mediaDosItens){
                qtdItens++;
            }
        }
        return qtdItens;
    }
}
