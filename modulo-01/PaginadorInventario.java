import java.util.ArrayList;

public class PaginadorInventario {
    private Inventario mochila;
    private int inicio;
    private int fim;

    public PaginadorInventario(Inventario mochila)
    {
        this.mochila = mochila;
        this.inicio = 0;
        this.fim = 0;
    }

    public void pular(int inicio)
    {
        this.inicio = (inicio > 0) ? inicio : this.inicio;
    }

    public ArrayList<Item> limitar(int fim)
    {
        ArrayList<Item> intervalo = new ArrayList<>();
        ArrayList<Item> itens = mochila.getItens();
        if(fim>0){
            this.fim = inicio+fim;
            for(int i=this.inicio; i<this.fim; i++){
                intervalo.add(itens.get(i));
            }
        }
        return intervalo;
    }
}
