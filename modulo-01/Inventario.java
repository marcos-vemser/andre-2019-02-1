import org.junit.Test;

import java.util.ArrayList;

public class Inventario
{
    private ArrayList<Item> itens;

    public Inventario()
    {
        this.itens = new ArrayList<>();
    }

    public void adicionar(Item item)
    {
        this.itens.add(item);
    }

    public Item obter(int posicao)
    {
        return this.itens.get(posicao);
    }

    public void remover(int posicao)
    {
        this.itens.remove(posicao);
    }

    public ArrayList<Item> getItens()
    {
        return this.itens;
    }

    public int size()
    {
        return this.itens.size();
    }

    public Item buscar(String descricao)
    {
        for(int i=0; i<size(); i++){
            if(this.itens.get(i).getDescricao().equals(descricao)){
                return this.itens.get(i);
            }
        }
        return null;
    }

    public String getDescricoesItens()
    {
        StringBuilder builder = new StringBuilder();
        for(int i=0; i<size(); i++){
            builder.append(this.itens.get(i).getDescricao());
            if(i < size()-1){
                builder.append(",");
            }
        }
        return builder.toString();
    }

    public Item getItemComMaiorQuantidade()
    {
        if(this.itens.size() > 0){
            Item maior = this.itens.get(0);
            for (int i = 1; i < this.itens.size(); i++) {
                if (this.itens.get(i) != null && maior != null) {
                    if (maior.getQuantidade() < this.itens.get(i).getQuantidade()) {
                        maior = this.itens.get(i);
                    }
                }
            }
            return maior;
        }
        return null;
    }

    public ArrayList<Item> inverter()
    {
        ArrayList<Item> inverso = new ArrayList<>();
        for(int i = size()-1; i>=0; i--){
            inverso.add(this.itens.get(i));
        }
        return inverso;
    }

    public void ordenarItens()
    {
        boolean trocou;
        do {
            trocou = false;
            for(int i=0; i<this.itens.size()-1; i++){
                if(this.itens.get(i).getQuantidade() > this.itens.get(i+1).getQuantidade()){
                    Item troca = this.itens.get(i);
                    this.itens.set(i, this.itens.get(i+1));
                    this.itens.set(i+1, troca);
                    trocou = true;
                }
            }
        }while(trocou);
    }

    public void ordenarItens(TipoOrdenacao tipoOrdenacao)
    {
        if(tipoOrdenacao.equals(TipoOrdenacao.ASC)){
            ordenarItens();
        }
        else{
            boolean trocou;
            do {
                trocou = false;
                for(int i=0; i<this.itens.size()-1; i++){
                    if(this.itens.get(i).getQuantidade() < this.itens.get(i+1).getQuantidade()){
                        Item troca = this.itens.get(i);
                        this.itens.set(i, this.itens.get(i+1));
                        this.itens.set(i+1, troca);
                        trocou = true;
                    }
                }
            }while(trocou);
        }
    }

}
