import java.util.ArrayList;

public class Elfo
{
    private String nome;
    private int experiencia;
    private Inventario mochila;
    private Status status;

    public Elfo(String nome)
    {
        this.nome = nome;
        this.experiencia = 0;
        this.status = Status.RECEM_CRIADO;
        this.mochila = new Inventario();
        this.mochila.adicionar(new Item(4, "Flecha"));
        this.mochila.adicionar(new Item(1, "Arco"));
    }

    public String getNome()
    {
        return this.nome;
    }

    public int getExperiencia()
    {
        return this.experiencia;
    }

    public Status getStatus()
    {
        return this.status;
    }

    public void aumentarXp()
    {
        this.experiencia++;
    }

    public Item getFlecha()
    {
        return mochila.buscar("Flecha");
    }

    public int getQtdFlechas()
    {
        return getFlecha().getQuantidade();
    }

    public boolean podeAtirarFlecha()
    {
        return getQtdFlechas() > 0;
    }

    public void atirarFlecha(Dwarf dwarf)
    {
        int qtdAtual = getQtdFlechas();
        if(podeAtirarFlecha()) {
            getFlecha().setQuantidade(qtdAtual - 1);
            dwarf.sofrerDano();
            aumentarXp();
        }
    }
}
