import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest
{
    @Test
    public void dwarfNasceCom110DeVida()
    {
        Dwarf dwarf = new Dwarf("Gimli");
        assertEquals(110.0, dwarf.getVida(), 1e-9);
    }

    @Test
    public void sofreDanoDiminuiDezNaVida()
    {
        Dwarf dwarf1 = new Dwarf("Gimli");
        dwarf1.sofrerDano();
        assertEquals(100.0, dwarf1.getVida(), 1e-9);
    }

    @Test
    public void dwarfPerdeTodaVida11Ataques()
    {
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<12; i++)
            dwarf1.sofrerDano();
        assertEquals(0, dwarf1.getVida(), 1e-9);
    }

    @Test
    public void decrementaVidaLimiteInferiorQuantidadeVida()
    {
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<12; i++)
            dwarf1.sofrerDano();
        assertEquals(0, dwarf1.getVida(), 1e-9);
    }

    @Test
    public void matarDwarfTrocaStatusParaMortoERetorna0()
    {
        Dwarf dwarf1 = new Dwarf("Gimli");
        for(int i=0; i<12; i++)
            dwarf1.sofrerDano();
        assertEquals(Status.MORTO, dwarf1.getStatus());
    }
}
